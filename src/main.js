import Component from './components/Component.js';
import Img from './components/Img.js';
const title = new Component('h1', null, ['La', ' ', 'carte']);
/*const img = new Img(
	'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
);*/

const c = new Component('article', { name: 'class', value: 'pizzaThumbnail' }, [
	new Img(
		'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	),
	'Regina',
]);

document.querySelector('.pageTitle').innerHTML = title.render();
//document.querySelector('.pageContent').innerHTML = img.render();
document.querySelector('.pageContent').innerHTML = c.render();
console.log(title.renderChildren());
