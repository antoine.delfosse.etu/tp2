class Component {
	tagName;
	attribute;
	children;
	constructor(tagName, attribute, children) {
		this.tagName = tagName;
		this.attribute = attribute;
		this.children = children;
	}

	render() {
		if (this.children != null) {
			return `<${this.tagName}>` + this.renderChildren() + `</${this.tagName}>`;
		} else {
			return `<${this.tagName} ` + this.renderAttribute() + ` />`;
		}
	}

	renderAttribute() {
		return `${this.attribute.name}="${this.attribute.value}"`;
	}

	renderChildren() {
		if (this.children instanceof Array) {
			let chaine = '';
			for (let i = 0; i < this.children.length; i++) {
				chaine += this.children[i];
			}
			return chaine;
		}
		//------------------------------------------
		else if (this.children instanceof Component) {
			if (this.children.children != null) {
				return (
					`<${this.children.tagName}>` +
					this.children.renderChildren() +
					`</${this.children.tagName}>`
				);
			} else {
				return (
					`<${this.children.tagName} ` + this.children.renderAttribute() + ` />`
				);
			}
		}
		//------------------------------------------
		else {
			return this.children;
		}
	}
}

export default Component;
