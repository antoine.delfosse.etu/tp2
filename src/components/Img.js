import Component from './Component.js';
class Img extends Component {
	image;
	constructor(image) {
		super('img', { name: 'src', value: image });
	}

	render() {
		return super.render();
	}
}

export default Img;
